var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';
var username = "ois.seminar";
var password = "ois4fri";
var tip = "";
var re = [
    ['Year', 'vrednost'],
    ['2014', 1170]
];


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
            "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */

function kliknagumb() {
    $("#generiranjePodatkov").html("");
    for (var i = 1; i < 4; i++)
        generirajPodatke(i);
}

function generirajPodatke(stPacienta) {
    ehrId = "";
    sessionId = getSessionId();

    if (stPacienta == 1) {
        var ime = "Timotej";
        var priimek = "Plaznik";
        var datumRojstva = "1968-12-12T07:12";
    }

    if (stPacienta == 2) {
        var ime = "Jure";
        var priimek = "Prijatelj";
        var datumRojstva = "1997-02-06T01:15";
    }

    if (stPacienta == 3) {
        var ime = "Larisa";
        var priimek = "Pečnik";
        var datumRojstva = "1998-11-11T04:15";
    }


    $.ajaxSetup({
        headers: {
            "Ehr-Session": sessionId
        }
    });
    $.ajax({
            url: baseUrl + "/ehr",
            type: 'POST',
            success: function(data) {
                var ehrId = data.ehrId;
                var partyData = {
                    firstNames: ime,
                    lastNames: priimek,
                    dateOfBirth: datumRojstva,
                    partyAdditionalInfo: [{
                        key: "ehrId",
                        value: ehrId
                    }, {
                        key: "email",
                        value: "test"
                    }]
                };
                $.ajax({
                        url: baseUrl + "/demographics/party",
                        type: 'POST',
                        contentType: 'application/json',
                        data: JSON.stringify(partyData),
                        success: function(party) {
                            if (party.action == 'CREATE') {
                                $("#generiranjePodatkov").append(" <span class='obvestilo " +
                                    "label label-success fade-in'>Uspešno kreiran EHR '" +
                                    ehrId + "'.<br></span>");
                                $("#preberiEHRid").val(ehrId);
                                //

                                var stevec = 0;
                                while (stPacienta == 1 && stevec < 6) {
                                    sessionId = getSessionId();
                                    stevec = stevec + 1;

                                    var datumInUra = "2014-11-21T11:40Z";
                                    var telesnaVisina = "180";
                                    var telesnaTeza = 80.0;
                                    var telesnaTemperatura = "32.7";
                                    var sistolicniKrvniTlak = "0";
                                    var diastolicniKrvniTlak = "0";
                                    var nasicenostKrviSKisikom = "0";
                                    var merilec = "Medicinska sestra Jelka";
                                    telesnaTeza = telesnaTeza + stevec;

                                    if (!ehrId || ehrId.trim().length == 0) {
                                        $("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
                                            "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
                                    }
                                    else {
                                        $.ajaxSetup({
                                            headers: {
                                                "Ehr-Session": sessionId
                                            }
                                        });
                                        var podatki = {
                                            // Struktura predloge je na voljo na naslednjem spletnem naslovu:
                                            // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
                                            "ctx/language": "en",
                                            "ctx/territory": "SI",
                                            "ctx/time": datumInUra,
                                            "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
                                            "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
                                            "vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
                                            "vital_signs/body_temperature/any_event/temperature|unit": "°C",
                                            "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
                                            "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
                                            "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
                                        };
                                        var parametriZahteve = {
                                            ehrId: ehrId,
                                            templateId: 'Vital Signs',
                                            format: 'FLAT',
                                            committer: merilec
                                        };
                                        $.ajax({
                                            url: baseUrl + "/composition?" + $.param(parametriZahteve),
                                            type: 'POST',
                                            contentType: 'application/json',
                                            data: JSON.stringify(podatki),
                                            success: function(res) {
                                                
                                            },
                                            error: function(err) {
                                                $("#dodajMeritveVitalnihZnakovSporocilo").html(
                                                    "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                                                    JSON.parse(err.responseText).userMessage + "'!");
                                            }
                                        });
                                    }
                                }

                                while (stPacienta == 2 && stevec < 6) {
                                    sessionId = getSessionId();
                                    stevec = stevec + 1;

                                    var datumInUra = "2015-12-21T11:50Z";
                                    var telesnaVisina = "178";
                                    var telesnaTeza = 82.0;
                                    var telesnaTemperatura = "37.7";
                                    var sistolicniKrvniTlak = "0";
                                    var diastolicniKrvniTlak = "0";
                                    var nasicenostKrviSKisikom = "0";
                                    var merilec = "Medicinska sestra Jelka";
                                    telesnaTeza = telesnaTeza + stevec;

                                    if (!ehrId || ehrId.trim().length == 0) {
                                        $("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
                                            "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
                                    }
                                    else {
                                        $.ajaxSetup({
                                            headers: {
                                                "Ehr-Session": sessionId
                                            }
                                        });
                                        var podatki = {
                                            // Struktura predloge je na voljo na naslednjem spletnem naslovu:
                                            // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
                                            "ctx/language": "en",
                                            "ctx/territory": "SI",
                                            "ctx/time": datumInUra,
                                            "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
                                            "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
                                            "vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
                                            "vital_signs/body_temperature/any_event/temperature|unit": "°C",
                                            "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
                                            "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
                                            "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
                                        };
                                        var parametriZahteve = {
                                            ehrId: ehrId,
                                            templateId: 'Vital Signs',
                                            format: 'FLAT',
                                            committer: merilec
                                        };
                                        $.ajax({
                                            url: baseUrl + "/composition?" + $.param(parametriZahteve),
                                            type: 'POST',
                                            contentType: 'application/json',
                                            data: JSON.stringify(podatki),
                                            success: function(res) {
                                               
                                            },
                                            error: function(err) {
                                                $("#dodajMeritveVitalnihZnakovSporocilo").html(
                                                    "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                                                    JSON.parse(err.responseText).userMessage + "'!");
                                            }
                                        });
                                    }
                                }

                                while (stPacienta == 3 && stevec < 6) {
                                    sessionId = getSessionId();
                                    stevec = stevec + 1;

                                    var datumInUra = "2016-12-21T11:50Z";
                                    var telesnaVisina = "185";
                                    var telesnaTeza = 89.0;
                                    var telesnaTemperatura = "36.7";
                                    var sistolicniKrvniTlak = "0";
                                    var diastolicniKrvniTlak = "0";
                                    var nasicenostKrviSKisikom = "0";
                                    var merilec = "Medicinska sestra Jelka";
                                    telesnaTeza = telesnaTeza + stevec;

                                    if (!ehrId || ehrId.trim().length == 0) {
                                        $("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
                                            "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
                                    }
                                    else {
                                        $.ajaxSetup({
                                            headers: {
                                                "Ehr-Session": sessionId
                                            }
                                        });
                                        var podatki = {
                                            // Struktura predloge je na voljo na naslednjem spletnem naslovu:
                                            // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
                                            "ctx/language": "en",
                                            "ctx/territory": "SI",
                                            "ctx/time": datumInUra,
                                            "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
                                            "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
                                            "vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
                                            "vital_signs/body_temperature/any_event/temperature|unit": "°C",
                                            "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
                                            "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
                                            "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
                                        };
                                        var parametriZahteve = {
                                            ehrId: ehrId,
                                            templateId: 'Vital Signs',
                                            format: 'FLAT',
                                            committer: merilec
                                        };
                                        $.ajax({
                                            url: baseUrl + "/composition?" + $.param(parametriZahteve),
                                            type: 'POST',
                                            contentType: 'application/json',
                                            data: JSON.stringify(podatki),
                                            success: function(res) {
                                                
                                            },
                                            error: function(err) {
                                                $("#dodajMeritveVitalnihZnakovSporocilo").html(
                                                    "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                                                    JSON.parse(err.responseText).userMessage + "'!");
                                            }
                                        });
                                    }
                                }
                            




                        }
                    },
                    error: function(err) {
                        $("#generiranjePodatkov").append(" <span class='obvestilo label " +
                            "label-danger fade-in'>Napaka '" +
                            JSON.parse(err.responseText).userMessage + "'!");
                    }
                });
        }
    });

return ehrId;
}



// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
function kreirajEHRzaBolnika() {
    sessionId = getSessionId();

    var ime = $("#kreirajIme").val();
    var priimek = $("#kreirajPriimek").val();
    var datumRojstva = $("#kreirajDatumRojstva").val();



    if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
        priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
        $("#kreirajSporocilo").html("<span class='obvestilo'>Prosimo vnesite vse podatke!</span>");
    }
    else {
        $.ajaxSetup({
            headers: {
                "Ehr-Session": sessionId
            }
        });
        $.ajax({
            url: baseUrl + "/ehr",
            type: 'POST',
            success: function(data) {
                var ehrId = data.ehrId;
                var partyData = {
                    firstNames: ime,
                    lastNames: priimek,
                    dateOfBirth: datumRojstva,
                    partyAdditionalInfo: [{
                        key: "ehrId",
                        value: ehrId
                    }]
                };
                $.ajax({
                    url: baseUrl + "/demographics/party",
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify(partyData),
                    success: function(party) {
                        if (party.action == 'CREATE') {
                            $("#kreirajSporocilo").html("<span class='obvestilo " +
                                "label label-success fade-in'>Uspešno kreiran EHR '" +
                                ehrId + "'.</span>");
                            $("#preberiEHRid").val(ehrId);
                        }
                    },
                    error: function(err) {
                        $("#kreirajSporocilo").html("<span class='obvestilo label " +
                            "label-danger fade-in'>Napaka '" +
                            JSON.parse(err.responseText).userMessage + "'!");
                    }
                });
            }
        });
    }
}


/**
 * Za podan EHR ID preberi demografske podrobnosti pacienta in izpiši sporočilo
 * s pridobljenimi podatki (ime, priimek in datum rojstva).
 */
function preberiEHRodBolnika() {
    sessionId = getSessionId();
    $("#preberiSporocilo").html("<span class='obvestilo'>");
    var ehrId = $("#preberiEHRid").val();


    if (!ehrId || ehrId.trim().length == 0) {
        $("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
            "fade-in'>Prosim vnesite zahtevan podatek!");
    }
    else {
        $.ajax({
            url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
            type: 'GET',
            headers: {
                "Ehr-Session": sessionId
            },
            success: function(data) {
                var party = data.party;
                var datum = party.dateOfBirth.substring(0, 10);
                var ura = party.dateOfBirth.substring(11, 16);
                console.log("Ura: " + ura);
                $("#prikaz").show();
                $("#test1").html("<span class='obvestilo'> " + party.firstNames + "</span>");
                $("#test11").html("<span class='obvestilo'>Ime: </span>");
                $("#test21").html("<span class='obvestilo'>Priimek: </span>");
                $("#test2").html("<span class='obvestilo'> " + party.lastNames + "</span>");
                $("#test31").html("<span class='obvestilo'>Datum in čas rojstva: </span>");
                $("#test3").html("<span class='obvestilo'> " + datum + " ob: " + ura + "</span>");
            },
            error: function(err) {
                $("#preberiSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
            }
        });

        izracunajBMI();


    }
}


/**
 * Za dodajanje vitalnih znakov pacienta je pripravljena kompozicija, ki
 * vključuje množico meritev vitalnih znakov (EHR ID, datum in ura,
 * telesna višina, telesna teža, sistolični in diastolični krvni tlak,
 * nasičenost krvi s kisikom in merilec).
 */

function dodajMeritveVitalnihZnakov() {
	sessionId = getSessionId();

	var ehrId = $("#dodajVitalnoEHR").val();
	var datumInUra = $("#dodajVitalnoDatumInUra").val();
	var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
	var telesnaTemperatura = $("#dodajVitalnoTelesnaTemperatura").val();
	var sistolicniKrvniTlak = $("#dodajVitalnoKrvniTlakSistolicni").val();
	var diastolicniKrvniTlak = $("#dodajVitalnoKrvniTlakDiastolicni").val();
	var nasicenostKrviSKisikom = $("#dodajVitalnoNasicenostKrviSKisikom").val();
	var merilec = $("#dodajVitalnoMerilec").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}


/**
 * Pridobivanje vseh zgodovinskih podatkov meritev izbranih vitalnih znakov
 * (telesna temperatura, filtriranje telesne temperature in telesna teža).
 * Filtriranje telesne temperature je izvedena z AQL poizvedbo, ki se uporablja
 * za napredno iskanje po zdravstvenih podatkih.
 */

function preberiMeritveVitalnihZnakov() {
    console.log("PREBERI MERITVE");
    document.getElementById('chart_div').style.visibility = 'visible';


    sessionId = getSessionId();
    re = [
        ['Year', 'vrednost'],
        ['2014', 1170]
    ];


    var ehrId = $("#meritveVitalnihZnakovEHRid").val();
    tip = $("#preberitip").val();
    console.log("TIP meritve; " + tip);
    var uspesno = 0;

    $("#preberiMeritveVitalnihZnakovSporocilo").html(
        "<span class='obvestilo label label-warning fade-in'>" +
        "</span>");

    if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
        $("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
            "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
    }
    else {
        $.ajax({
            url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
            type: 'GET',
            headers: {
                "Ehr-Session": sessionId
            },
            success: function(data) {
                var party = data.party;
                if (tip == "telesna temperatura") {
                    $("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
                        "podatkov za <b>'" + tip + "'</b> bolnika <b>'" + party.firstNames +
                        " " + party.lastNames + "'</b>.</span><br/><br/>");
                    uspesno = 1;
                    $.ajax({
                        url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",
                        type: 'GET',
                        headers: {
                            "Ehr-Session": sessionId
                        },
                        success: function(res) {
                            if (res.length > 0) {
                                var results = "<table class='table table-striped " +
                                    "table-hover'><tr><th>Datum in ura</th>" +
                                    "<th class='text-right'>Telesna temperatura</th></tr>";
                                var stevec = 1;
                                for (var i in res) {

                                    re[stevec] = [res[i].time.substring(2, 10), res[i].temperature];
                                    results += "<tr><td>" + res[i].time +
                                        "</td><td class='text-right'>" + res[i].temperature +
                                        " " + res[i].unit + "</td>";
                                    stevec = stevec + 1;
                                }
                                results += "</table>";
                                $("#rezultatMeritveVitalnihZnakov").append(results);
                                var options = {
                                    title: 'test123',
                                    hAxis: {
                                        title: 'Datum123',
                                        titleTextStyle: {
                                            color: '#333'
                                        }
                                    },
                                    vAxis: {
                                        minValue: 0
                                    },
                                    backgroundColor: '#FAFAFA'
                                };
                                drawChart(data, options);
                            }
                            else {
                                $("#preberiMeritveVitalnihZnakovSporocilo").html(
                                    "<span class='obvestilo label label-warning fade-in'>" +
                                    "Ni podatkov!</span>");
                                document.getElementById('chart_div').style.visibility = 'hidden';
                            }
                        },
                        error: function() {
                            $("#preberiMeritveVitalnihZnakovSporocilo").html(
                                "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                                JSON.parse(err.responseText).userMessage + "'!");
                            document.getElementById('chart_div').style.visibility = 'hidden';
                        }
                    });
                }
                else if (tip == "telesna teza") {
                    $("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
                        "podatkov za <b>'" + tip + "'</b> bolnika <b>'" + party.firstNames +
                        " " + party.lastNames + "'</b>.</span><br/><br/>");
                    uspesno = 1;
                    $.ajax({
                        url: baseUrl + "/view/" + ehrId + "/" + "weight",
                        type: 'GET',
                        headers: {
                            "Ehr-Session": sessionId
                        },
                        success: function(res) {
                            if (res.length > 0) {
                                var results = "<table class='table table-striped " +
                                    "table-hover'><tr><th>Datum in ura</th>" +
                                    "<th class='text-right'>Telesna teža</th></tr>";
                                var stevec = 1;
                                for (var i in res) {
                                    re[stevec] = [res[i].time.substring(2, 10), res[i].weight];
                                    results += "<tr><td>" + res[i].time +
                                        "</td><td class='text-right'>" + res[i].weight + " " +
                                        res[i].unit + "</td>";
                                    stevec = stevec + 1;
                                }
                                results += "</table>";
                                $("#rezultatMeritveVitalnihZnakov").append(results);
                                var options = {
                                    title: tip,
                                    hAxis: {
                                        title: 'Datum',
                                        titleTextStyle: {
                                            color: '#333'
                                        }
                                    },
                                    vAxis: {
                                        minValue: 0
                                    },
                                    backgroundColor: '#FAFAFA'
                                };
                                drawChart(data, options);
                            }
                            else {
                                $("#preberiMeritveVitalnihZnakovSporocilo").html(
                                    "<span class='obvestilo label label-warning fade-in'>" +
                                    "Ni podatkov!</span>");
                                document.getElementById('chart_div').style.visibility = 'hidden';
                            }
                        },
                        error: function() {
                            $("#preberiMeritveVitalnihZnakovSporocilo").html(
                                "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                                JSON.parse(err.responseText).userMessage + "'!");
                            document.getElementById('chart_div').style.visibility = 'hidden';
                        }
                    });
                }
                else if (tip == "nasicenost-krvi") {
                    $("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
                        "podatkov za <b>'" + tip + "'</b> bolnika <b>'" + party.firstNames +
                        " " + party.lastNames + "'</b>.</span><br/><br/>");
                    uspesno = 1;

                    $.ajax({
                        url: baseUrl + "/view/" + ehrId + "/" + "spO2",
                        type: 'GET',
                        headers: {
                            "Ehr-Session": sessionId
                        },
                        success: function(res) {
                            var results = "<table class='table table-striped " +
                                "table-hover'><tr><th>Datum in ura</th>" +
                                "<th class='text-right'>Nasicenost krvi s kisikom</th></tr>";
                            if (res.length > 0) {

                                //console.log("rezultati" + rows);
                                var stevec = 1;
                                for (var i in res) {
                                    re[stevec] = [res[i].time.substring(2, 10), res[i].spO2];
                                    results += "<tr><td>" + res[i].time +
                                        "</td><td class='text-right'>" + res[i].spO2 + "% " + "</td>";
                                    stevec = stevec + 1;
                                }
                                results += "</table>";
                                $("#rezultatMeritveVitalnihZnakov").append(results);
                                var options = {
                                    title: tip,
                                    hAxis: {
                                        title: 'Datum',
                                        titleTextStyle: {
                                            color: '#333'
                                        }
                                    },
                                    vAxis: {
                                        minValue: 0
                                    },
                                    backgroundColor: '#FAFAFA'
                                };
                                drawChart(data, options);
                            }
                            else {
                                $("#preberiMeritveVitalnihZnakovSporocilo").html(
                                    "<span class='obvestilo label label-warning fade-in'>" +
                                    "Ni podatkov!</span>");
                                document.getElementById('chart_div').style.visibility = 'hidden';
                            }

                        },
                        error: function() {
                            $("#preberiMeritveVitalnihZnakovSporocilo").html(
                                "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                                JSON.parse(err.responseText).userMessage + "'!");
                            document.getElementById('chart_div').style.visibility = 'hidden';
                        }
                    });


                }
                if (uspesno == 0) $("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ta vrsta meritev ne obstaja, preveri njen zapis!</span>");
            },
            error: function(err) {
                $("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
            }

        });
    }
}

$(document).ready(function() {
    $('#preberiObstojeciEHR').change(function() {
        $("#preberiSporocilo").html("");
        $("#dodajVitalnoEHR").html("");
        $("#meritveVitalnihZnakovEHRid").html("");
        $("#preberiEHRid").val($(this).val());
        $("#dodajVitalnoEHR").val($(this).val());
        $("#meritveVitalnihZnakovEHRid").val($(this).val());
    });
    document.getElementById('map').style.visibility = 'hidden';
    document.getElementById('chart_div').style.visibility = 'hidden';



});

google.charts.load('current', {
    'packages': ['corechart']
});
google.charts.setOnLoadCallback(drawChart);


function drawChart() {





    var data = google.visualization.arrayToDataTable(re);

    //var data = google.visualization.arrayToDataTable(re);
    //console.log(data);


    var options = {
        title: tip,
        hAxis: {
            title: 'Datum',
            titleTextStyle: {
                color: '#333'
            }
        },
        vAxis: {
            minValue: 0
        },
        backgroundColor: '#FAFAFA'
    };


    var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
    chart.draw(data, options);

}




function izracunajBMI() {
    sessionId = getSessionId();
    var ehrId = $("#preberiEHRid").val();
    re = [
        ['Year', 'vrednost'],
        ['2014', 1170]
    ];
var visina = 0;
                        

    var ehrId = $("#meritveVitalnihZnakovEHRid").val();
    tip = $("#preberitip").val();
    var uspesno = 0;

    $("#preberiMeritveVitalnihZnakovSporocilo").html(
        "<span class='obvestilo label label-warning fade-in'>" +
        "</span>");

    var teza = 0;
    //telesna teza
    $.ajax({
        url: baseUrl + "/view/" + ehrId + "/" + "weight",
        type: 'GET',
        headers: {
            "Ehr-Session": sessionId
        },
        success: function(res) {
            if (res.length > 0) {
                for (var i in res) {
                    teza = res[i].weight;
                    console.log("Teza znasa: " + teza);
                }
            }
            else {
                $("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
            }
        },
        error: function() {
            $("#preberiMeritveVitalnihZnakovSporocilo").html(
                "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                JSON.parse(err.responseText).userMessage + "'!");
        }
    });


    // telesna visina

    $.ajax({
        url: baseUrl + "/view/" + ehrId + "/" + "height",
        type: 'GET',
        headers: {
            "Ehr-Session": sessionId
        },
        success: function(res) {
            if (res.length > 0) {
                for (var i in res) {
                    visina = res[i].height;
                    console.log("Visina znasa: " + visina);

                }

                console.log("Zadnja teza " + teza + " " + ", zadnja visina: " + visina);
                if (teza != undefined && visina != undefined) {
                    var BMI = (teza / ((visina / 100) * (visina / 100)));
                    BMI = Math.round(BMI * 100) / 100;

                    if (BMI < 18.5) {
                        $("#test41").html("<span class='obvestilo'>Zadnji izračunan BMI: </span>");
                        $("#test4").html("<span class='obvestilo' style='color:red'> " + BMI + "</span>");
                    }

                    else if (BMI < 25) {
                        $("#test41").html("<span class='obvestilo'>Zadnji izračunan BMI: </span>");
                        $("#test4").html("<span class='obvestilo' style='color:green'> " + BMI + "</span>");
                    }

                    else if (BMI < 30) {
                        $("#test41").html("<span class='obvestilo'>Zadnji izračunan BMI: </span>");
                        $("#test4").html("<span class='obvestilo' style='color:orange'> " + BMI + "</span>");
                    }

                    else if (BMI < 30) {
                        $("#test41").html("<span class='obvestilo'>Zadnji izračunan BMI: </span>");
                        $("#test4").html("<span class='obvestilo' style='color:red'> " + BMI + "</span>");
                    }


                }
                else {
                    $("#test4").html("<span class='obvestilo' style='color:red'>  NI DOVOLJ PODATKOV! </span>");

                    izracunajBMI();
                }
            }
            else {
                $("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");

            }
        },
        error: function() {
            $("#preberiMeritveVitalnihZnakovSporocilo").html(
                "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                JSON.parse(err.responseText).userMessage + "'!");

        }
    });



}

var map;
var infowindow;

var x = document.getElementById("demo");

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    }
    else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

var xe = 0;
var ye = 0;

function showPosition(position) {
    console.log("position: " + position);
    console.log(position.coords.latitude + " - " + position.coords.longitude);
    xe = position.coords.latitude;
    ye = position.coords.longitude;
    initMap();
    document.getElementById('map').style.visibility = 'visible';

}


function initMap() {
    var pyrmont = {
        lat: xe,
        lng: ye
    };


    map = new google.maps.Map(document.getElementById('map'), {
        center: pyrmont,
        zoom: 15
    });

    infowindow = new google.maps.InfoWindow();
    var service = new google.maps.places.PlacesService(map);
    service.nearbySearch({
        location: pyrmont,
        radius: 15000,
        type: ['hospital']
    }, callback);
}

function callback(results, status) {
    console.log("Status: " + status);
    if (status === google.maps.places.PlacesServiceStatus.OK) {
        for (var i = 0; i < results.length; i++) {
            console.log(results[i]);
            createMarker(results[i]);
        }
    }
}

function createMarker(place) {
    var placeLoc = place.geometry.location;
    var marker = new google.maps.Marker({
        map: map,
        position: place.geometry.location
    });

    google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(place.name);
        infowindow.open(map, this);
    });
}

google.charts.load('current', {
    'packages': ['corechart']
});
google.charts.setOnLoadCallback(drawChart);
